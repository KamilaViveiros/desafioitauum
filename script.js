const meses = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
let table = document.querySelector('tbody')
let lancamentos = [];

const fatura = [
    { descricao: 'Uber', valor: 1430.00, mesLancamento: 12 },
    { descricao: 'PS Store', valor: 40.00, mesLancamento: 03 },
    { descricao: 'GPlay', valor: 37.50, mesLancamento: 03 },
    { descricao: 'Uber', valor: 1382.00, mesLancamento: 03 },
    { descricao: 'iFood', valor: 77.00, mesLancamento: 02 },
    { descricao: 'Uber', valor: 3.00, mesLancamento: 01 },
    { descricao: 'iFood', valor: 20.34, mesLancamento: 01 },
    { descricao: 'iFood', valor: 37.40, mesLancamento: 01 },
    { descricao: 'Uber', valor: 17.70, mesLancamento: 03 },
    { descricao: 'GPlay', valor: 16.70, mesLancamento: 03 },
    { descricao: 'AppStore', valor: 15.00, mesLancamento: 02 },
    { descricao: 'Uber', valor: 96.11, mesLancamento: 02 },
    { descricao: 'GPlay', valor: 7.71, mesLancamento: 02 },
    { descricao: 'AppStore', valor: 3.33, mesLancamento: 02 },
    { descricao: 'iFood', valor: 47.98, mesLancamento: 03 },
    { descricao: 'AppStore', valor: 12.22, mesLancamento: 03 },
    { descricao: 'Uber', valor: 2.56, mesLancamento: 04 },
    { descricao: 'Uber', valor: 5.32, mesLancamento: 03 },
    { descricao: 'PS Store', valor: 5.44, mesLancamento: 03 },
    { descricao: 'PS Store', valor: 98.37, mesLancamento: 03 },
    { descricao: 'PS Store', valor: 78.90, mesLancamento: 01 },
    { descricao: 'GPlay', valor: 65.03, mesLancamento: 03 },
    { descricao: 'iFood', valor: 32.12, mesLancamento: 03 },
    { descricao: 'Uber', valor: 34.56, mesLancamento: 01 },
    { descricao: 'iFood', valor: 1480.00, mesLancamento: 03 },
    { descricao: 'Uber', valor: 5.34, mesLancamento: 03 },
    { descricao: 'iFood', valor: 6.12, mesLancamento: 01 },
    { descricao: 'Uber', valor: 344.12, mesLancamento: 03 },
    { descricao: 'GPlay', valor: 96.10, mesLancamento: 03 },
    { descricao: 'Uber', valor: 6.09, mesLancamento: 01 },
    { descricao: 'PS Store', valor: 3.21, mesLancamento: 03 }
];





function agruparLancamentos() {
    for (let indice = 0; indice < fatura.length; indice++) { 
        adicionarLancamento(fatura[indice])
    }
}
function adicionarLancamento(lancamento) {
    existeLancameto(lancamento)
}

function existeLancameto(lancamento) {
    for (let indice = 0; indice < lancamentos.length; indice++) {
        if (lancamentos[indice].mes == lancamento.mesLancamento) {
            lancamentos[indice].total += lancamento.valor
            return;
        }
    }
    addLancamento(lancamento);
}

function addLancamento(lancamento){
    lancamentos.push({ mes: lancamento.mesLancamento, total: lancamento.valor })
}

function preencherTabela(){
    agruparLancamentos();
    table.innerHTML = lancamentos.sort((a,b) => a.mes > b.mes ? 1 : -1 ).map((lancamento) => `<tr><td>${meses[lancamento.mes-1]}</td><td>R$ ${lancamento.total.toFixed(2)}</td></tr>`).join(" ");
}

window.onload = () => preencherTabela();